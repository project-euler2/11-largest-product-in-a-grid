grid='08022297381500400075040507785212507791084949994017811857608717409843694804566200814931735579142993714067538830034913366552709523046011426924685601325671370236912231167151676389419236542240402866331380244732609903450244753353783684203517125032988128642367102638406759547066183864706726206802621220956394396308409166499421245558056673992697177878968314883489637221362309750076442045351400613397343133957817532822753167159403800462161409535692163905429635314755588824001754243629855786560048357189070544443744602158515417581980816805944769287392138652177704895540045208839735991607975732162626793327986688366887576220720346336746551232639353690442167338253911249472180846293240627636206936417230238834629969826759857404361620733529783190017431497148868116235705540170547183515469169233486143520189196748'

def product_in_grid(dimension_grid, grid):
    max_product = 0
    #Loop that computes the left right products
    for j in range(0,len(grid) - 6,dimension_grid*2):
        for i in range(0,dimension_grid*2 - 6,2):
            if (int(grid[i+j:i+j+2]) * int(grid[i+j+2:i+j+4]) * int(grid[i+j+4:i+j+6]) * int(grid[i+j+6:i+j+8])) > max_product:
                max_product = (int(grid[i+j:i+j+2]) * int(grid[i+j+2:i+j+4]) * int(grid[i+j+4:i+j+6]) * int(grid[i+j+6:i+j+8]))
    #Loop that computes the top to bottom products --> has been thorougly checked
    for i in range(0, len(grid) - dimension_grid*6,2):
        if (int(grid[i:i+2]) * int(grid[i+dimension_grid*2:i+dimension_grid*2+2]) * int(grid[i+dimension_grid*4:i+dimension_grid*4+2]) * int(grid[i+dimension_grid*6:i+dimension_grid*6+2])) > max_product:
           max_product = (int(grid[i:i+2]) * int(grid[i+dimension_grid*2:i+dimension_grid*2+2]) * int(grid[i+dimension_grid*4:i+dimension_grid*4+2]) * int(grid[i+dimension_grid*6:i+dimension_grid*6+2]))
    #Loop that computes the right diagonal products
    for j in range(0,len(grid) - 6*dimension_grid,dimension_grid*2):
        for i in range(0, dimension_grid*2 - 6,2):
            if (int(grid[i+j:i+j+2]) * int(grid[i+j+2+dimension_grid*2:i+j+dimension_grid*2+4]) * int(grid[i+j+4+dimension_grid*4:i+j+dimension_grid*4+6]) * int(grid[i+j+6+dimension_grid*6:i+j+dimension_grid*6+8])) > max_product:
                max_product = (int(grid[i+j:i+j+2]) * int(grid[i+j+2+dimension_grid*2:i+j+dimension_grid*2+4]) * int(grid[i+j+4+dimension_grid*4:i+j+dimension_grid*4+6]) * int(grid[i+j+6+dimension_grid*6:i+j+dimension_grid*6+8]))
    #Loop that computes the left diagonal products
    for j in range(0,len(grid) - 6*dimension_grid,dimension_grid*2):
        for i in range(0, dimension_grid*2 - 6,2):
            if (int(grid[i+j+6:i+j+8]) * int(grid[i+j+4+dimension_grid*2:i+j+dimension_grid*2+6]) * int(grid[i+j+2+dimension_grid*4:i+j+dimension_grid*4+4]) * int(grid[i+j+dimension_grid*6:i+j+dimension_grid*6+2])) > max_product:
                max_product = (int(grid[i+j+6:i+j+8]) * int(grid[i+j+4+dimension_grid*2:i+j+dimension_grid*2+6]) * int(grid[i+j+2+dimension_grid*4:i+j+dimension_grid*4+4]) * int(grid[i+j+dimension_grid*6:i+j+dimension_grid*6+2]))
    return max_product
print(product_in_grid(20,grid))